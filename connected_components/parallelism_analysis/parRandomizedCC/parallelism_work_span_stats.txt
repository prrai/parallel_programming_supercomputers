cilkview: generating scalability data
Cilkview Scalability Analyzer V2.0.0, Build 2516
Graph created..
Total vertices: 3997962
Total components: 1
Input file: com-lj-in.txt, Output file: out_ran.txt, Execution time: 2704.59

Whole Program Statistics
1) Parallelism Profile
   Work :					 2,351,429,399,210 instructions
   Span :					 365,952,092,327 instructions
   Burdened span :				 366,416,761,120 instructions
   Parallelism :				 6.43
   Burdened parallelism :			 6.42
   Number of spawns/syncs:			 7,288,201,592
   Average instructions / strand :		 107
   Strands along span :				 39,381
   Average instructions / strand on span :	 9,292,605
   Total number of atomic instructions : 	 7,288,207,876
   Frame count :				 14,576,403,184

2) Speedup Estimate
     2 processors:	 1.58 - 2.00
     4 processors:	 2.23 - 4.00
     8 processors:	 2.80 - 6.43
    16 processors:	 3.22 - 6.43
    32 processors:	 3.47 - 6.43
    64 processors:	 3.62 - 6.43
   128 processors:	 3.69 - 6.43
   256 processors:	 3.73 - 6.43

Cilk Parallel Region(s) Statistics - Elapsed time: 23.830 seconds
1) Parallelism Profile
   Work :					 1,985,482,433,165 instructions
   Span :					 5,126,282 instructions
   Burdened span :				 469,795,075 instructions
   Parallelism :				 387314.32
   Burdened parallelism :			 4226.27
   Number of spawns/syncs:			 7,288,201,592
   Average instructions / strand :		 90
   Strands along span :				 19,690
   Average instructions / strand on span :	 260
   Total number of atomic instructions : 	 7,288,207,876
   Frame count :				 14,576,403,184
   Entries to parallel region :			 1,468

2) Speedup Estimate
     2 processors:	 1.90 - 2.00
     4 processors:	 3.80 - 4.00
     8 processors:	 7.60 - 8.00
    16 processors:	 15.20 - 16.00
    32 processors:	 30.40 - 32.00
    64 processors:	 60.80 - 64.00
   128 processors:	 121.60 - 128.00
   256 processors:	 232.18 - 256.00
