# Uncomment 2 lines below to create a .png file instead of displaying using X
set terminal png size 600, 600
set output "parallel_RandomizedCC_scalibility_plot.png"
# Set aspect ration, zoom factor
set size square 1.0, 1.0
# Set plot title
set title "Trial results for Cilk Parallel Region(s)"
# Set X, Y axis titles
set xlabel "Worker Count"
set ylabel "Speedup"
# Specify where key is to be displayed
set key left top box
# Make points a bit bigger than the defaultset pointsize 1.8
set xrange [0:16]
set yrange [0:16]
# Plot the data
plot x title 'Parallelism' lt 2, \
9.591603 notitle lt 2, \
'-' title 'Burdened Parallelism' with lines lt 1
1 1.000000
2 1.698377
3 2.213713
4 2.609630
5 2.923329
6 3.178010
7 3.388898
8 3.566392
9 3.717844
10 3.848592
11 3.962611
12 4.062918
13 4.151847
14 4.231229
15 4.302523
16 4.366906
e
