#include <iostream>
#include <vector>
#include <unordered_map>
//#include <cilk/cilk.h>

using namespace std;

void display(vector<int>& res) {
	for (int i = 0; i < res.size(); i++)
		cout<<res[i]<<" ";
	cout<<endl;
}

void find_roots(int n, vector<int> &S, vector<int>& C) {
	vector<int> temp(C.size());
	int ctr = 0, sz = C.size();
	while (sz != 0) {
		ctr = 0;
		cilk_for (int i = 0; i < sz; i++) {
			if (S[C[i]] != S[S[C[i]]]) {
				S[C[i]] = S[S[C[i]]];
				temp[ctr++] = i;
			}
		}
		sz = ctr;
		C.swap(temp);
	}
}

int main() {
	int vertices, ip;
	vector<int> labels;
	vector<int> unresolved;
	cin>>vertices;
	for (int i = 0; i < vertices; i++) {
		cin>>ip;
		labels.push_back(ip);
		unresolved.push_back(i);
	}
	find_roots(vertices, labels, unresolved);
	display(labels);
	return 0;
}
