#include <iostream>
#include <vector>
#include <unordered_map>
#include <cilk/cilk.h>

using namespace std;

void display(vector<int>& res) {
	for (int i = 0; i < res.size(); i++)
		cout<<res[i]<<" ";
	cout<<endl;
}

void find_roots(int n, vector<int> &S) {
	bool flag = true;
	while (flag) {
		flag = false;
		cilk_for (int v = 0; v < n; v++) {
			S[v] = S[S[v]];
			if (S[v] != S[S[v]])
				flag = true;
		}
	}
}

int main() {
	int vertices, ip;
	vector<int> labels;
	cin>>vertices;
	for (int i = 0; i < vertices; i++) {
		cin>>ip;
		labels.push_back(ip);
	}
	find_roots(vertices, labels);
	display(labels);
	return 0;
}
