#include <iostream>
#include <vector>
#include <cilk/cilk.h>

using namespace std;

void display(vector<int>& res) {
	for (int i = 0; i < res.size(); i++)
		cout<<res[i]<<" ";
	cout<<endl;
}

inline int pow2roundup (int x)
{
    if (x < 0)
        return 0;
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x+1;
}


vector<int> prefix_sums_util(vector<int> x, int n, vector<int>& s) {
	vector<int> y(n/2, 0);
	vector<int> z(n/2, 0);
	if (n == 1)
		s[1] = x[1];
	else {
		cilk_for (int i = 1; i < n/2; i++)
			y[i] = x[2*i-1] + x[2*i];
		z = prefix_sums_util(y, n/2, s);
		cilk_for (int i = 1; i < n; i++) {
			if (i == 1)
				s[1] = x[1];
			else if (i%2 == 0)
				s[i] = z[i/2];
			else
				s[i] = z[(i-1)/2] + x[i];
		}
	}
	return s;
}

vector<int> prefix_sums(vector<int> x) {
	int n = x.size();
	int next_pow = pow2roundup(n + 1);
	int zeroes = next_pow - n;
	vector<int> temp(zeroes, 0);
	temp.insert(temp.end(), x.begin(), x.end());
	vector<int> s(temp.size(), 0);
	s = prefix_sums_util(temp, temp.size(), s);
	s.erase(s.begin(), s.begin() + zeroes);
	return s;
}

int par_sum(vector<int> x) {
	vector<int> res = prefix_sums(x);
	return res[res.size() - 1];
}

int main() {
	int n, ip;
	vector<int> nums, res;
	cin>>n;
	while (n--) {
		cin>>ip;
		nums.push_back(ip);
	}
	res = prefix_sums(nums);
	display(res);
	int r = par_sum(nums);
	cout<<r<<endl;
	return 0;
}