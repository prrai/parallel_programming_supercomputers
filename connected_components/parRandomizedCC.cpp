#include <iostream>
#include <vector>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <unordered_map>
#include <queue>
#include <stack>
#include <cilk/cilk.h>
#include <algorithm>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

using namespace std;

vector<int> L;
vector<int> C;
vector<int> S;

void display(vector<int>& res) {
	for (int i = 0; i < res.size(); i++)
		cout<<res[i]<<" ";
	cout<<endl;
}

void displayPairs(vector<pair<int, int> >& res) {
	for (int i = 0; i < res.size(); i++)
		cout<<res[i].first<<" "<<res[i].second<<endl;
}

inline int pow2RoundUp(int x) {
    if (x < 0)
        return 0;
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x+1;
}

vector<int> prefixSumsUtil(vector<int> x, int n, vector<int>& s) {
	vector<int> y(n/2, 0);
	vector<int> z(n/2, 0);
	if (n == 1)
		s[1] = x[1];
	else {
		cilk_for (int i = 1; i < n/2; i++)
			y[i] = x[2*i-1] + x[2*i];
		z = prefixSumsUtil(y, n/2, s);
		cilk_for (int i = 1; i < n; i++) {
			if (i == 1)
				s[1] = x[1];
			else if (i%2 == 0)
				s[i] = z[i/2];
			else
				s[i] = z[(i-1)/2] + x[i];
		}
	}
	return s;
}

void createGraphFromInput(string fileLocation, int& vertices, vector<pair<int, int> >& E) {  
            int from, to;
            string line;
            ifstream myfile;
            int a, b, e;

            myfile.open(fileLocation);

            getline(myfile, line);
            istringstream iss(line);
            iss >> vertices >> e;

            while (getline(myfile, line)) {
                istringstream iss(line);
                if (!(iss >> a >> b)) { continue; }
                E.push_back(make_pair(a, b));
                E.push_back(make_pair(b, a));
            }
            cout<<"Graph created..\n";
            cout<<"Total vertices: "<<vertices<<endl;
			vertices++;
}

vector<int> prefixSums(vector<int>& x) {
	int n = x.size();
	int next_pow = pow2RoundUp(n + 1);
	int zeroes = next_pow - n;
	vector<int> temp(zeroes, 0);
	temp.insert(temp.end(), x.begin(), x.end());
	vector<int> s(temp.size(), 0);
	s = prefixSumsUtil(temp, temp.size(), s);
	s.erase(s.begin(), s.begin() + zeroes);
	return s;
}


int parSum(vector<int>& in) {
	vector<int> x = prefixSums(in);
	return x.size() > 0 ? x[x.size() - 1] : 0;
}


void findRoots(int n) {
	bool flag = true;
	while (flag) {
		flag = false;
		for (int v = 0; v < n; v++) {
			L[v] = L[L[v]];
			if (L[v] != L[L[v]])
				flag = true;
		}
	}
}


vector<int> parRandomizedCC(int n, vector<pair<int, int> > E) {
	S.resize(E.size());
	vector<int> M(n);
	if (E.size() == 0) {
		return L;
	}

	cilk_for (int i=0; i< n; i++) {
        C[i] = rand() % 2;
    }

    cilk_for (int i = 0; i < E.size(); i++) {
        /* 
         * if first is tails and second heads
         */
        if (C[E[i].first] == 0 && C[E[i].second] == 1) {
            L[E[i].first] = L[E[i].second];
        }
    }

    cilk_for (int i=0; i<E.size(); i++) {
        if (L[E[i].first] != L[E[i].second]) {
            S[i] = 1;
        } else {
            S[i] = 0;
        }
    }

    S = prefixSums(S);

	vector<pair<int, int> > F(S[S.size() - 1]);
	cilk_for (int i = 0; i < E.size(); i++)
		if (L[E[i].first] != L[E[i].second])
			F[S[i] - 1] = make_pair(L[E[i].first], L[E[i].second]);
 
	M = parRandomizedCC(n, F);
	cilk_for (int i=0; i<E.size(); i++) {
        if (E[i].second == L[E[i].first]) {
            M[E[i].first] = M[E[i].second];
        }
    }
    return M;
}

void dump_to_file(const char* file, unordered_map<int, int>& counts) {
	std::vector<std::pair<int, int> > pairs;
	ofstream stream(file);
	priority_queue<int> sorted_comp;
	for (auto it = counts.begin(); it != counts.end(); it++)
	  sorted_comp.push(it->second);
	stream << counts.size() << '\n';
	while (!sorted_comp.empty()) {
		stream << sorted_comp.top()<<'\n';
		sorted_comp.pop();
	}	
	stream.close();
}


int main(int argc, char* argv[]) {
	int vertices;
	double execution_time;
	struct timeval start, end;
	vector<pair<int, int> > edges;
	srand(static_cast<int>(time(0)));
	createGraphFromInput(argv[1], vertices, edges);
	for (int i = 0; i < vertices; i++)
		L.push_back(i);
	C.resize(vertices);
	gettimeofday(&start, NULL);
	L = parRandomizedCC(vertices, edges);
	gettimeofday(&end,NULL);
	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
	unordered_map<int, int> counts;	
	for (int i = 0; i < L.size(); i++)
	  counts[L[i]]++;
	counts.erase(0);
	cout<<"Total components: "<<counts.size()<<endl;
	cout<<"Input file: "<<argv[1]<<", Output file: "<<argv[2]<<", Execution time: "<<execution_time<<endl;
	dump_to_file(argv[2], counts);
	return 0;
}
