1. Compile the program on Stampede using icpc:
	icpc <cpp_program> -o <obj_file> -std=c++0x
	
	Example:
		icpc parDeterministic.cpp -o parDet -std=c++0x

2. Execute the program:
	./<obj_file> <input_file> <output_file>

	Example:
		./parDet com-youtube-in.txt det-com-youtube-out.txt

	Above execution would run DeterministicCC algorithm on the input file: ‘com-	youtube-in.txt’ and dump the result to the output file: ‘det-com-youtube-		out.txt’

	
3. Input/Output Files:
	The input file must follow the format:
	<Number of vertices = n> <Number of edges = m>
	<V1> <`V1>		// Edge exists between V1 and `V1
	<V2> <`V2> 
	…
	…
	…
	<Vm> <`Vm>

	The output file follows the format:
	<Number of connected components = k>
	<Size of component1>	// Descending order in the number of vertices
	<Size of component2>
	…
	…
	…
	<Size of componentk>

4. Submission files:
	1. parDeterministic.cpp =>  Program for parDeterministicCC algorithm.
	2. parRandomizedCC.cpp	 =>  Program for parRandomizedCC algorithm.
	3. parDeterministicOptimizedFIndRoots.cpp => Program for parDeterministicCC 	   that uses optimized FindRoots implementation
	4. Files in helpers directory: These are test programs for different modules
          in the algorithms 1. , 2. and 3.
	5. Files in outputs directory => Outputs produced for runs on datasets from A
	   Appendix 1.
	6. tabulations.docx => Tabulation data collected from experiments.
	7. *.png => Terminal screenshots from our runs.
	8. Group9_CSE613_Assignment2_writeup.pdf => PDF report for the submission.
	9. README.txt => This README file.
	10. Files in partE directory => The scalability plots are related data for 		    the two parallel CC implementations.
	 



	
