#include <iostream>
#include <mpi.h>
#include <sstream>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <cstdio>
#include <sys/time.h>
#include <cmath>

using namespace std;

static unsigned int seed_val;

inline void seeder(int seed_it) {
	seed_val = seed_it;
}

inline int fast_rand() {
	seed_val = (214013 * seed_val + 2531011);
	return (seed_val >> 16) & 0x7FFF;
}

int free2D(int ***array) {
    free(&((*array)[0][0]));
    free(*array);
    return 0;
}

int **allocateMatrix(int edge) {
	int *data = (int *)malloc(edge*edge*sizeof(int));
   	int **array= (int **)malloc(edge*sizeof(int*));
   	for (int i=0; i<edge; i++)
       		array[i] = &(data[edge*i]);
	return array;
}


void writeMatrixToFile(int **mat, string fileName, int N) {	
	ofstream f(fileName.c_str());
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			f << mat[i][j] << ' ';
		}
		f << '\n';
	}
}
void serialMultiply(int **first, int **second, int **third, int sz) {
	for (int i = 0; i < sz; i++) {
		for (int j = 0; j < sz; j++) {
			for (int k = 0; k < sz; k++) {
				third[i][j] += first[i][k] * second[k][j];
			}
		}
	}
}

pair<int, int> mapProcess1DTo2D(int id, int p, int blockSize) {
	pair<int, int> position;
	position.first = id / (int)sqrt(p);
	position.second = (id % (int)sqrt(p));
	return position;
}

int mapProcess2Dto1D(pair<int, int> position, int p) {
	return (int)sqrt(p) * position.first + position.second;
}

int getRankofDistantNeighbour(int id, int N, int p, int blockSize, int dist, int dir, int axis, pair<int, int> position) {
	
	pair<int, int> neighPosition;
	if (axis == 0) {
		neighPosition.first = position.first;
		if (position.second + (dist * dir) >= 0)
			neighPosition.second = (position.second + (dist * dir)) % (int)sqrt(p);
		else
			neighPosition.second = ((int)sqrt(p) + ((dist * dir) + position.second)) % (int)sqrt(p);
	}
	else {
		neighPosition.second = position.second;
		if (position.first + (dist * dir) >= 0)
			neighPosition.first = (position.first + (dist * dir)) % (int)sqrt(p);
		else
			neighPosition.first = ((int)sqrt(p) + ((dist * dir) + position.first)) % (int)sqrt(p);
	}
	return mapProcess2Dto1D(neighPosition, p);
}


void performInitialAlignment(int N, int **a, int **b, int processes, MPI_Comm comm, int processRank, int blockSize, pair<int, int> position) {
	MPI_Status status;
	int toProcess, fromProcess;
	
	toProcess =  getRankofDistantNeighbour(processRank, N, processes, blockSize,position.first, -1, 0, position);
	fromProcess = getRankofDistantNeighbour(processRank, N, processes, blockSize, position.first, 1, 0, position);
	//printf("myRank: %d, toProcess: %d, fromProcess: %d \n", processRank, toProcess, fromProcess);
	//if (fromProcess != processRank && toProcess != processRank)
		MPI_Sendrecv_replace(&(a[0][0]), blockSize*blockSize, MPI_INT, toProcess, 1, fromProcess, 1, comm, &status);
	toProcess =  getRankofDistantNeighbour(processRank, N, processes, blockSize,position.second, -1, 1, position);
	fromProcess = getRankofDistantNeighbour(processRank, N, processes, blockSize, position.second, 1, 1, position);	
	// printf("myRank: %d, toProcess: %d, fromProcess: %d \n", processRank, toProcess, fromProcess);	
	//if (fromProcess != processRank && toProcess != processRank)
		MPI_Sendrecv_replace(&(b[0][0]), blockSize*blockSize, MPI_INT, toProcess, 1, fromProcess, 1, comm, &status);
}

void exchangeDataWithNeighbours(int N, int **a, int **b, int **c, MPI_Comm comm, int processes, int blockSize, int processRank, pair<int, int> position) {
	MPI_Status status;
	int processLeftNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, -1, 0, position);
	int processRightNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, 1, 0, position);
	int processTopNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, -1, 1, position);
	int processBottomNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, 1, 1, position);
	//printf("rank: %d, left: %d, right: %d, top: %d, bottom: %d\n", processRank, processLeftNeighbour, processRightNeighbour, processTopNeighbour, processBottomNeighbour);
	for (int proc = 0; proc < (int)sqrt(processes); proc++) {
		serialMultiply(a, b, c, blockSize);
		MPI_Sendrecv_replace(&(a[0][0]), blockSize*blockSize, MPI_INT, processLeftNeighbour, 1, processRightNeighbour, 1, comm, &status);
		MPI_Sendrecv_replace(&(b[0][0]), blockSize*blockSize, MPI_INT, processTopNeighbour, 1, processBottomNeighbour, 1, comm, &status);
	}
}

void cannonMatrixMultiplication(int n, int **a, int **b, int **c, MPI_Comm comm, int processes, int blockSize, int processRank)
{
	pair<int, int> position = mapProcess1DTo2D(processRank, processes, blockSize);
	//printf("1D: %d, 2Dx: %d, 2Dy: %d\n", processRank, position.first, position. second);	
	performInitialAlignment(n, a, b, processes, comm, processRank, blockSize, position);
	//printf("Exiting initial alignment\n");
	exchangeDataWithNeighbours(n, a, b, c, comm, processes, blockSize, processRank, position);
}

int **fillEmptyMatrix(int edge) {
	int **mat = allocateMatrix(edge);
	for (int i = 0; i < edge; i++)
		for (int j = 0; j < edge; j++)
			mat[i][j] = 0;
	return mat;
}

int **fillRandomMatrix(int edge) {
	int **mat = allocateMatrix(edge);
	for (int i = 0; i < edge; i++) {
		for (int j = 0; j < edge; j++) {
			mat[i][j] = fast_rand() % 200 + 1;
			mat[i][j] -= 100;
		}
	}
	return mat;
}


void distributeInitialBlocks(int **first, int **second, int **third, int edge, int p, int blockSize, int totalBlocks) {
	for (int proc = 1; proc < totalBlocks; proc++) {
		int **A = allocateMatrix(blockSize);
		int **B = allocateMatrix(blockSize);
		int **C = allocateMatrix(blockSize);
		int startX = blockSize * (proc / (int)sqrt(p));
		int startY = blockSize * (proc % (int)sqrt(p));
	//	printf("p=%d, blockSize=%d, Rank=%d, startX=%d, startY=%d\n", p, blockSize, proc, startX, startY);
		for (int i = startX; i < startX + blockSize; i++) { 
			for (int j = startY; j < startY + blockSize; j++) {
				A[i - startX][j - startY] = first[i][j];
				B[i - startX][j - startY] = second[i][j];
				C[i - startX][j - startY] = third[i][j];
			}
		}
		MPI_Send(&(A[0][0]), blockSize * blockSize, MPI_INT, proc, 1, MPI_COMM_WORLD);
		MPI_Send(&(B[0][0]), blockSize * blockSize, MPI_INT, proc, 1, MPI_COMM_WORLD);
		MPI_Send(&(C[0][0]), blockSize * blockSize, MPI_INT, proc, 1, MPI_COMM_WORLD);
		free2D(&A);
		free2D(&B);
		free2D(&C);
	}
}

int main(int argc, char* argv[]) {
	double executionTime;
	double startTime, endTime;
	seeder(time(NULL));
	MPI_Status status;
	if (argc < 3 || atoi(argv[1]) < 1 || atoi(argv[2]) < 0) {
		cout<<"[Usage]: matMul <n :: 2^n x 2^n => matrix size> <l :: 2^l x 2^l => number_of_compute_nodes (mpi resources)>"<<endl;
		return 1;
	}
	int **first = NULL;
	int **second = NULL;
	int **block_A = NULL;
	int **block_B = NULL;
	int **block_C = NULL;
	int **result = NULL;
	int N = pow(2, atoi(argv[1]));
	int p = pow(2, atoi(argv[2]));
	p *= p;
	int totalBlocks = p;
	int blockSize = N / sqrt(p);
	int processRank = 0;
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &processRank);

	block_A = allocateMatrix(blockSize);
	block_B = allocateMatrix(blockSize);
	block_C = allocateMatrix(blockSize);
	startTime = MPI_Wtime();
	if (processRank == 0) {
		first = fillRandomMatrix(N);
		result = fillEmptyMatrix(N);
		second = fillRandomMatrix(N);
		//writeMatrixToFile(first, "MPI_mat_first.txt", N);
		//writeMatrixToFile(second, "MPI_mat_second.txt", N);
		distributeInitialBlocks(first, second, result, N, p, blockSize, totalBlocks);
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {
				block_A[i][j] = first[i][j];
				block_B[i][j] = second[i][j];
				block_C[i][j] = result[i][j];
			}
		}
	}
	else {
		MPI_Recv(&(block_A[0][0]), blockSize * blockSize, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&(block_B[0][0]), blockSize * blockSize, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&(block_C[0][0]), blockSize * blockSize, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	cannonMatrixMultiplication(N, block_A, block_B, block_C, MPI_COMM_WORLD, p, blockSize, processRank);
	MPI_Barrier(MPI_COMM_WORLD);
	if (processRank != 0)
		MPI_Send(&(block_C[0][0]), blockSize * blockSize, MPI_INT, 0, processRank, MPI_COMM_WORLD);
	else {
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {
				result[i][j] = block_C[i][j];
			}
		}
		for (int proc = 1; proc < totalBlocks; proc++) {
			int **C = allocateMatrix(blockSize);
			MPI_Recv(&(C[0][0]), blockSize * blockSize, MPI_INT, proc, proc, MPI_COMM_WORLD, &status);		
			int startX = blockSize * (proc / (int)sqrt(p));
			int startY = blockSize * (proc % (int)sqrt(p));
		//	printf("p=%d, blockSize=%d, Rank=%d, startX=%d, startY=%d\n", p, blockSize, proc, startX, startY);
			for (int i = startX; i < startX + blockSize; i++) { 
				for (int j = startY; j < startY + blockSize; j++) {
					result[i][j] = C[i - startX][j - startY];
				}
			}
			free2D(&C);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	endTime = MPI_Wtime();
	if (processRank == 0) {
		executionTime = endTime - startTime;
	 	printf("k: %d, l: %d, Execution time: %f\n", atoi(argv[1]), atoi(argv[2]), executionTime);
		// string fileName = "MPI_mat_result.txt";
		// writeMatrixToFile(result, fileName, N);
	 /*	int ** ground = fillEmptyMatrix(N);
	 	serialMultiply(first, second, ground, N);
	 	writeMatrixToFile(ground, "MPI_mat_ground.txt", N);	*/
	 }		
	MPI_Finalize();
}
