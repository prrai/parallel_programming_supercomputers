#include <iostream>
#include <mpi.h>
#include <sstream>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <cilk/cilk.h>
#include <cstdio>
#include <sys/time.h>
#include <cmath>

using namespace std;

int seed_val;
int GRANULARITY = pow(2, 8);

void seeder(int seed_it) {
	seed_val = seed_it;
}

int fast_rand() {
	seed_val = (214013 * seed_val + 2531011);
	return (seed_val >> 16) & 0x7FFF;
}

int free2D(int ***array) {
    free(&((*array)[0][0]));
    free(*array);
    return 0;
}

int **allocateMatrix(int edge) {
	int *data = (int *)malloc(edge*edge*sizeof(int));
   	int **array= (int **)malloc(edge*sizeof(int*));
   	for (int i=0; i<edge; i++)
       		array[i] = &(data[edge*i]);
	return array;
}


void writeMatrixToFile(int **mat, string fileName, int N) {	
	ofstream f(fileName.c_str());
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			f << mat[i][j] << ' ';
		}
		f << '\n';
	}
}

void multiplyMatrixIterative(int n, int **matX, int **matY, int **matZ, pair<int, int> startX, pair<int, int> startY, pair<int, int> startZ) {
	for (int i = 0; i < n; i++)
		for (int k = 0; k < n; k++)
			for (int j = 0; j < n; j++)
				matZ[i + startZ.first][j + startZ.second] = matZ[i + startZ.first][j + startZ.second] + (matX[i + startX.first][startX.second + k] * matY[startY.first + k][startY.second + j]);
}

void multiplyMatrixDivideAndConquer(int n, int **matX, int **matY, int **matZ, pair<int, int> startX, pair<int, int> startY, pair<int, int> startZ) {
	if (n == GRANULARITY) {
		multiplyMatrixIterative(n, matX, matY, matZ, startX, startY, startZ);
		return;
	}
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, startX, startY, startZ);
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, startX, make_pair(startY.first, startY.second + n/2), make_pair(startZ.first, startZ.second + n/2));
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second), startY, make_pair(startZ.first + n/2, startZ.second));
	multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second), make_pair(startY.first, startY.second + n/2), make_pair(startZ.first + n/2, startZ.second + n/2));

    cilk_sync;

    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first, startX.second + n/2), make_pair(startY.first + n/2, startY.second), startZ);
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first, startX.second + n/2), make_pair(startY.first + n/2, startY.second + n/2), make_pair(startZ.first, startZ.second + n/2));
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startY.first + n/2, startY.second), make_pair(startZ.first + n/2, startZ.second));
	multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startY.first + n/2, startY.second + n/2), make_pair(startZ.first + n/2, startZ.second + n/2));

	cilk_sync;
}


pair<int, int> mapProcess1DTo2D(int id, int p, int blockSize) {
	pair<int, int> position;
	position.first = id / (int)sqrt(p);
	position.second = (id % (int)sqrt(p));
	return position;
}

int mapProcess2Dto1D(pair<int, int> position, int p) {
	return (int)sqrt(p) * position.first + position.second;
}

int getRankofDistantNeighbour(int id, int N, int p, int blockSize, int dist, int dir, int axis, pair<int, int> position) {
	
	pair<int, int> neighPosition;
	if (axis == 0) {
		neighPosition.first = position.first;
		if (position.second + (dist * dir) >= 0)
			neighPosition.second = (position.second + (dist * dir)) % (int)sqrt(p);
		else
			neighPosition.second = ((int)sqrt(p) + ((dist * dir) + position.second)) % (int)sqrt(p);
	}
	else {
		neighPosition.second = position.second;
		if (position.first + (dist * dir) >= 0)
			neighPosition.first = (position.first + (dist * dir)) % (int)sqrt(p);
		else
			neighPosition.first = ((int)sqrt(p) + ((dist * dir) + position.first)) % (int)sqrt(p);
	}
	return mapProcess2Dto1D(neighPosition, p);
}


void performInitialAlignment(int N, int **a, int **b, int processes, MPI_Comm comm, int processRank, int blockSize, pair<int, int> position) {
	MPI_Status status;
	int toProcess, fromProcess;
	
	toProcess =  getRankofDistantNeighbour(processRank, N, processes, blockSize,position.first, -1, 0, position);
	fromProcess = getRankofDistantNeighbour(processRank, N, processes, blockSize, position.first, 1, 0, position);
	//printf("myRank: %d, toProcess: %d, fromProcess: %d \n", processRank, toProcess, fromProcess);
	//if (fromProcess != processRank && toProcess != processRank)
		MPI_Sendrecv_replace(&(a[0][0]), blockSize*blockSize, MPI_INT, toProcess, 1, fromProcess, 1, comm, &status);
	toProcess =  getRankofDistantNeighbour(processRank, N, processes, blockSize,position.second, -1, 1, position);
	fromProcess = getRankofDistantNeighbour(processRank, N, processes, blockSize, position.second, 1, 1, position);	
	// printf("myRank: %d, toProcess: %d, fromProcess: %d \n", processRank, toProcess, fromProcess);	
	//if (fromProcess != processRank && toProcess != processRank)
		MPI_Sendrecv_replace(&(b[0][0]), blockSize*blockSize, MPI_INT, toProcess, 1, fromProcess, 1, comm, &status);
}

void exchangeDataWithNeighbours(int N, int **a, int **b, int **c, MPI_Comm comm, int processes, int blockSize, int processRank, pair<int, int> position) {
	MPI_Status status;
	int processLeftNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, -1, 0, position);
	int processRightNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, 1, 0, position);
	int processTopNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, -1, 1, position);
	int processBottomNeighbour = getRankofDistantNeighbour(processRank, N, processes, blockSize, 1, 1, 1, position);
	//printf("rank: %d, left: %d, right: %d, top: %d, bottom: %d\n", processRank, processLeftNeighbour, processRightNeighbour, processTopNeighbour, processBottomNeighbour);
	for (int proc = 0; proc < (int)sqrt(processes); proc++) {
		multiplyMatrixDivideAndConquer(blockSize, a, b, c, make_pair(0, 0), make_pair(0, 0), make_pair(0, 0));
		MPI_Sendrecv_replace(&(a[0][0]), blockSize*blockSize, MPI_INT, processLeftNeighbour, 1, processRightNeighbour, 1, comm, &status);
		MPI_Sendrecv_replace(&(b[0][0]), blockSize*blockSize, MPI_INT, processTopNeighbour, 1, processBottomNeighbour, 1, comm, &status);
	}
}

void cannonMatrixMultiplication(int n, int **a, int **b, int **c, MPI_Comm comm, int processes, int blockSize, int processRank)
{
	pair<int, int> position = mapProcess1DTo2D(processRank, processes, blockSize);
	//printf("1D: %d, 2Dx: %d, 2Dy: %d\n", processRank, position.first, position. second);	
	performInitialAlignment(n, a, b, processes, comm, processRank, blockSize, position);
	//printf("Exiting initial alignment\n");
	exchangeDataWithNeighbours(n, a, b, c, comm, processes, blockSize, processRank, position);
}

int **fillEmptyMatrix(int edge) {
	int **mat = allocateMatrix(edge);
	for (int i = 0; i < edge; i++)
		for (int j = 0; j < edge; j++)
			mat[i][j] = 0;
	return mat;
}

int **fillRandomMatrix(int edge) {
	int **mat = allocateMatrix(edge);
	for (int i = 0; i < edge; i++) {
		for (int j = 0; j < edge; j++) {
			mat[i][j] = fast_rand() % 200 + 1;
			mat[i][j] -= 100;
		}
	}
	return mat;
}


int main(int argc, char* argv[]) {
	double executionTime;
	double startTime, endTime;
	if (argc < 3 || atoi(argv[1]) < 1 || atoi(argv[2]) < 0) {
		cout<<"[Usage]: matMul <n :: 2^n x 2^n => matrix size> <l :: 2^l x 2^l => number_of_compute_nodes (mpi resources)>"<<endl;
		return 1;
	}
	if (atoi(argv[1]) < 9) {
		cout<<"Since we use GRANULARITY of 2^8 in divideAndConquer matrix multiplication, kindly provide matrix of atleast size 2^9"<<endl;
		return 1;
	}
	int N = pow(2, atoi(argv[1]));
	int p = pow(2, atoi(argv[2]));
	p *= p;
	int totalBlocks = p;
	int blockSize = N / sqrt(p);
	int processRank = 0;
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &processRank);
	int seed = static_cast<int>(time(0)) + processRank;	
	seeder(seed);
	int **block_A = fillRandomMatrix(blockSize);
	int **block_B = fillRandomMatrix(blockSize);
	int **block_C = fillEmptyMatrix(blockSize);
	//stringstream ss;
	//ss << processRank;
	//string fileName = "ip_" + string(ss.str()); 
	//writeMatrixToFile(block_A, fileName + "_A.txt" ,blockSize);
	//writeMatrixToFile(block_B, fileName + "_B.txt" ,blockSize);
	// writeMatrixToFile(C, fileName + "_C.txt" ,blockSize);
	MPI_Barrier(MPI_COMM_WORLD);
	startTime = MPI_Wtime();
	cannonMatrixMultiplication(N, block_A, block_B, block_C, MPI_COMM_WORLD, p, blockSize, processRank);
	MPI_Barrier(MPI_COMM_WORLD);	
	endTime = MPI_Wtime();
	executionTime = endTime - startTime;
	//fileName = "op_" + string(ss.str());
	//writeMatrixToFile(block_C, fileName + "_C.txt" ,blockSize);
	if (processRank == 0) {
		printf("k: %d, l: %d, Execution time: %f\n", atoi(argv[1]), atoi(argv[2]), executionTime);
	}
	MPI_Finalize();
}
