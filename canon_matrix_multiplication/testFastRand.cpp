#include <iostream>
#include <sstream>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <cstdio>
#include <sys/time.h>
#include <cmath>

using namespace std;

/* This is a tester program to compare rand() and fast_rand().
We have utilized fast_rand() implementation from intel's paper:
https://software.intel.com/en-us/articles/fast-random-number-generator-on-the-intel-pentiumr-4-processor/
*/

int **allocateMatrix(int edge) {
	int *data = (int *)malloc(edge*edge*sizeof(int));
   	int **array= (int **)malloc(edge*sizeof(int*));
   	for (int i=0; i<edge; i++)
       		array[i] = &(data[edge*i]);
	return array;
}

void writeMatrixToFile(int **mat, string fileName, int N) {	
	ofstream f(fileName.c_str());
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			f << mat[i][j] << ' ';
		}
		f << '\n';
	}
}

static unsigned int g_seed;

inline void fast_srand(int seed) {
	g_seed = seed;
}

inline int fastrand() {
	g_seed = 214013 *  g_seed + 2531011;
	return (g_seed >> 16) & 0x7FFF;
}

int **fillRandomMatrixF(int edge) {
	int **mat = allocateMatrix(edge);
	for (int i = 0; i < edge; i++) {
		for (int j = 0; j < edge; j++) {
			mat[i][j] = fastrand() % 200 + 1;
			mat[i][j] -= 100;
		}
	}
	return mat;
}

int **fillRandomMatrix(int edge) {
	int **mat = allocateMatrix(edge);
	for (int i = 0; i < edge; i++) {
		for (int j = 0; j < edge; j++) {
			mat[i][j] = rand() % 200 + 1;
			mat[i][j] -= 100;
		}
	}
	return mat;
}

int main(int argc, char* argv[]) {
	int N = pow(2, atoi(argv[1]));
	fast_srand(time(NULL));
	double execution_time;
	struct timeval start, end;
	gettimeofday(&start, NULL);
	int **test = fillRandomMatrix(N);
	gettimeofday(&end,NULL);
	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
	cout<<"N: "<<N<<" Execution Time: "<<execution_time<<endl;
	writeMatrixToFile(test, "MPI_mat_first.txt", N);

	fast_srand(time(NULL));
	gettimeofday(&start, NULL);
	int **test2 = fillRandomMatrix(N);
	gettimeofday(&end,NULL);
	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
	cout<<"N: "<<N<<" Execution Time: "<<execution_time<<endl;
	writeMatrixToFile(test2, "MPI_mat_second.txt", N);

	return 0;
}