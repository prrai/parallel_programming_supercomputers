#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH --nodes=4	           # Number of nodes, not cores	
#SBATCH --ntasks-per-node=4              # total number of mpi tasks requested
#SBATCH -p compute         # queue (partition) -- normal, development, etc.
#SBATCH -t 06:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
#SBATCH --export=ALL
ibrun ./partD 10 1
ibrun ./partD 11 1
ibrun ./partD 12 1
ibrun ./partD 13 1 
