#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH --nodes=1	           # Number of nodes, not cores	
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH -p compute         # queue (partition) -- normal, development, etc.
#SBATCH -t 06:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
#SBATCH --export=ALL
ibrun ./partA 10 0
ibrun ./partA 11 0
ibrun ./partA 12 0
ibrun ./partA 13 0 
