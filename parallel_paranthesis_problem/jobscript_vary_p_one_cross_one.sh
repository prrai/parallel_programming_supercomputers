#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 32              # total number of mpi tasks requested
#SBATCH -p normal         # queue (partition) -- normal, development, etc.
#SBATCH -t 25:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
./ParanThesis 13 1 2
./ParanThesis 13 1 4
./ParanThesis 13 1 6
./ParanThesis 13 1 8
./ParanThesis 13 1 16
