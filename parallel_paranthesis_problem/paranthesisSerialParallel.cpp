#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <vector>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <cilk/reducer_min.h>
#include <time.h>
#include <sys/time.h>

using namespace std;


vector<vector<int> > generateRandomMatrix(int n) {
	vector<vector<int> > mat(n, vector<int>(n, 0));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (j == i + 1)
				mat[i][j] = rand() % 1000 + 1;
			else
				mat[i][j] = 9999;
		}
	}
	return mat;
}

void writeMatrixToFile(vector<vector<int> > mat, string filename) {
	ofstream f(filename.c_str());
	for(vector<vector<int> >::const_iterator it = mat.begin(); it != mat.end(); it++) {
		for(vector<int>::const_iterator it2 = it->begin(); it2 != it->end(); it2++) {
			f << *it2 << ' ';
		}
		f << '\n';
	}
}

void serialParanthesis(vector<vector<int> > matX, int n, int startX) {
	double execution_time;
	struct timeval start, end;

	gettimeofday(&start,NULL);
	for (int i = n - 1; i >=  startX; i--)
		for (int j = i + 2; j < n; j++)
			for (int k = i + 1; k < j; k++)
				matX[i][j] = min(matX[i][j], matX[i][k] + matX[k][j]);
	gettimeofday(&end,NULL);
	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
		cout<<"Algorithm: Serial algorithm, n = "<<n<<", Execution Time: "<<
		execution_time<<endl;
		writeMatrixToFile(matX, "Result_serial.txt");
}


void parallelParanthesis(vector<vector<int> > matX, int n, int startX) {
	double execution_time;
	struct timeval start, end;

	gettimeofday(&start,NULL);
	for (int i = n - 1; i >= startX; i--)
		for (int j = i + 2; j < n; j++) {
			cilk::reducer< cilk::op_min<int> > min_val;
			cilk_for (int k = i + 1; k < j; k++) {
				min_val->calc_min(matX[i][k] + matX[k][j]);
			}
			matX[i][j] = min(matX[i][j], min_val.get_value());
		}
	gettimeofday(&end,NULL);
	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);

	cout<<"Algorithm: Parallelized serial algorithm, n = "<<n<<", Execution Time: "<<
	execution_time<<endl;
	writeMatrixToFile(matX, "Result_parallel.txt");
}


int main(int argc, char* argv[]) {
	if (argc < 2 || atoi(argv[1]) < 1 || atoi(argv[2]) < 1) {
		cout<<"[Usage]: matMul <n :: 2^n => matrix edge> <m :: 2^m => base_case_matrix_edge_length>"<<endl;
		return 1;
	}
	int n = pow(2, atoi(argv[1]));
	srand(static_cast<int>(time(0)));
	vector<vector<int> > matX = generateRandomMatrix(n);
	writeMatrixToFile(matX, "matX.txt");
	serialParanthesis(matX, n, 0);
	parallelParanthesis(matX, n, 0);
	return 0;
}

