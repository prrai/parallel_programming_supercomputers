#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <vector>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <cilk/reducer_min.h>
#include <time.h>
#include <sys/time.h>

using namespace std;

int GRANULARITY = 1; // Global variable, overriden from command line.

vector<vector<int> > generateRandomMatrix(int n) {
	vector<vector<int> > mat(n, vector<int>(n, 0));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (j == i + 1)
				mat[i][j] = rand() % 1000 + 1;
			else
				mat[i][j] = 9999;
		}
	}
	return mat;
}

void writeMatrixToFile(vector<vector<int> > mat, string filename) {
	ofstream f(filename.c_str());
	for(vector<vector<int> >::const_iterator it = mat.begin(); it != mat.end(); it++) {
		for(vector<int>::const_iterator it2 = it->begin(); it2 != it->end(); it2++) {
			f << *it2 << ' ';
		}
		f << '\n';
	}
}

int GRANULARTY = 1;
void serialParanthesisDnC(vector<vector<int> >& matX, int n, pair<int, int> startX) {
	for (int i = n - 1; i >= 0; i--)
		for (int j = i + 2; j < n; j++)
			for (int k = i + 1; k < j; k++)
				matX[startX.first + i][startX.second + j] = min(matX[startX.first + i][startX.second + j], matX[startX.first + i][startX.second + k] + matX[startX.first + k][startX.second + j]);
}


void A_base_kernel(vector<vector<int> >& matX, int n, pair<int, int> startX) {
	serialParanthesisDnC(matX, n, startX);
}

void B_base_kernel(vector<vector<int> >& matX, int n, pair<int, int> startX, pair<int, int> startU, pair<int, int> startV) {
	for (int x = n - 1; x >= 0; x--) {
		for (int i = x; i < n; i++) {
			int j = i - x;
			for (int k = i; k < n; k++) {
				matX[startX.first + i][startX.second + j] = min(matX[startX.first + i][startX.second + j], matX[startU.first + i][startU.second + k] + matX[startV.first + k][startV.second + j]);
			}
			for (int k = 0; k < j; k++) {
				matX[startX.first + i][startX.second + j] = min(matX[startX.first + i][startX.second + j], matX[startU.first + i][startU.second + k] + matX[startV.first + k][startV.second + j]);
			}
		}
	}
}

void C_base_kernel(vector<vector<int> >& matX, int n, pair<int, int> startX, pair<int, int> startU, pair<int, int> startV) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			for (int k = 0; k < n; k++) {
				matX[startX.first + i][startX.second + j] = min(matX[startX.first + i][startX.second + j], matX[startU.first + i][startU.second + k] + matX[startV.first + k][startV.second + j]);
			}
		}
	}
}



 void C_par(vector<vector<int> >& matX, int n, pair<int, int> startX, pair<int, int> startU, pair<int, int> startV) {
 	if (n == GRANULARTY) {
 		C_base_kernel(matX, n, startX, startU, startV);
 		return;
 	}
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first, startX.second), make_pair(startU.first, startU.second), make_pair(startV.first, startV.second));
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first, startX.second + n/2), make_pair(startU.first, startU.second), make_pair(startV.first, startV.second + n/2));
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first + n/2, startX.second), make_pair(startU.first + n/2, startU.second), make_pair(startV.first, startV.second));
 	C_par(matX, n/2, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startU.first + n/2, startU.second), make_pair(startV.first, startV.second + n/2));
 	cilk_sync;
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first, startX.second), make_pair(startU.first, startU.second + n/2), make_pair(startV.first + n/2, startV.second));
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first, startX.second + n/2), make_pair(startU.first, startU.second + n/2), make_pair(startV.first + n/2, startV.second + n/2));
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first + n/2, startX.second), make_pair(startU.first + n/2, startU.second + n/2), make_pair(startV.first + n/2, startV.second));
 	C_par(matX, n/2, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startU.first + n/2, startU.second + n/2), make_pair(startV.first + n/2, startV.second + n/2));
 	cilk_sync;
 }

 void B_par(vector<vector<int> >& matX, int n, pair<int, int> startX, pair<int, int> startU, pair<int, int> startV) {
 	if (n == GRANULARTY) {
 		B_base_kernel(matX, n, startX, startU, startV);
 		return;
 	}
 	B_par(matX, n/2, make_pair(startX.first + n/2, startX.second), make_pair(startU.first + n/2, startU.second + n/2), make_pair(startV.first, startV.second));
 	cilk_spawn C_par(matX, n/2, make_pair(startX.first, startX.second), make_pair(startU.first, startU.second + n/2), make_pair(startX.first + n/2, startX.second));
 	C_par(matX, n/2, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startX.first + n/2, startX.second), make_pair(startV.first, startV.second + n/2));
 	cilk_sync;
 	cilk_spawn B_par(matX, n/2, make_pair(startX.first, startX.second), make_pair(startU.first, startU.second), make_pair(startV.first, startV.second));
 	B_par(matX, n/2, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startU.first + n/2, startU.second + n/2), make_pair(startV.first + n/2, startV.second + n/2));
 	cilk_sync;
 	C_par(matX, n/2, make_pair(startX.first, startX.second + n/2), make_pair(startU.first, startU.second + n/2), make_pair(startX.first + n/2, startX.second + n/2));
 	C_par(matX, n/2, make_pair(startX.first, startX.second + n/2), make_pair(startX.first, startX.second), make_pair(startV.first, startV.second + n/2));
 	B_par(matX, n/2, make_pair(startX.first, startX.second + n/2), make_pair(startU.first, startU.second), make_pair(startV.first + n/2, startV.second + n/2));
 }

 void A_par(vector<vector<int> >& matX, int n, pair<int, int> startX) {
 	if (n == GRANULARITY) {
 		A_base_kernel(matX, n, startX);
 		return;
 	}
 	cilk_spawn A_par(matX, n/2, make_pair(startX.first, startX.second));
 	A_par(matX, n/2, make_pair(startX.first + n/2, startX.second + n/2));
 	cilk_sync;
 	B_par(matX, n/2, make_pair(startX.first, startX.second + n/2), make_pair(startX.first, startX.second), make_pair(startX.first + n/2, startX.second + n/2));
 }

 void parallelParanthesisDC(vector<vector<int> > matX, int n) {
 	double execution_time;
	struct timeval start, end;
 	gettimeofday(&start, NULL);
 	A_par(matX, n, make_pair(0, 0));
 	gettimeofday(&end, NULL);
 	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);

 	cout<<"Algorithm: Parallel Divide and Conquer, n = "<<n<<", m = "<<GRANULARITY<< ", Execution Time: "<<
 	execution_time<<endl;
 	writeMatrixToFile(matX, "Result_parallel_DC.txt");
 }

int main(int argc, char* argv[]) {
	if (argc < 2 || atoi(argv[1]) < 1 || atoi(argv[2]) < 1) {
		cout<<"[Usage]: matMul <n :: 2^n => matrix edge> <m :: 2^m => base_case_matrix_edge_length>"<<endl;
		return 1;
	}
	int n = pow(2, atoi(argv[1]));
	GRANULARITY = pow(2, atoi(argv[2]));
	srand(static_cast<int>(time(0)));
	vector<vector<int> > matX = generateRandomMatrix(n);
	writeMatrixToFile(matX, "matX.txt");
	parallelParanthesisDC(matX, n);
	return 0;
}

