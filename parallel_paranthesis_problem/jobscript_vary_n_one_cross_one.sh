#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 32              # total number of mpi tasks requested
#SBATCH -p normal         # queue (partition) -- normal, development, etc.
#SBATCH -t 25:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
./ParanThesis 4 1
./ParanThesis 5 1
./ParanThesis 6 1
./ParanThesis 7 1
./ParanThesis 8 1
./ParanThesis 9 1
./ParanThesis 10 1
./ParanThesis 11 1
./ParanThesis 12 1
./ParanThesis 13 1
