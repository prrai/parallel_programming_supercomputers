#include <iostream>
#include <vector>
#include <cilk/cilk.h>
#include <fstream>
#include <cilk/cilk_api.h>

using namespace std;

void sieve(vector<bool>& primes, int n) {
	primes[0] = primes[1] = false;
	primes[2] = true;
	for (int i = 3; i < n; i++)
		primes[i] = true;
	for (int i = 2; i * i < n; i++) {
		if (primes[i]) {
			for (int j = 2 * i; j < n; j+=i)
				primes[j] = false;
		}
	}
}

void parallel_sieve(vector<bool>& primes, int n) {
	primes[0] = primes[1] = false;
	primes[2] = true;
	parallel_for (int i = 3; i < n; i++)
		primes[i] = true;
	parallel_for (int i = 2; i * i < n; i++) {
		if (primes[i]) {
			parallel_for (int j = 2 * i; j < n; j+=i)
				primes[j] = false;
		}
	}
}

void write(vector<bool>& primes, int n, string filename) {
	ofstream f(filename.c_str());
	for (int i = 0; i < n; i++)
		if (primes[i])
			f<<i<<" ";
}

int main() {
	int n;
	cin>>n;
	vector<bool> primes(n + 1, true);
	sieve(primes, n);
	write(primes, n, "serial_primes.txt");
	parallel_sieve(primes, n);
	write(primes, n, "parallel_primes.txt");
	return 0;
}