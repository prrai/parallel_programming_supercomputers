#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 32              # total number of mpi tasks requested
#SBATCH -p development         # queue (partition) -- normal, development, etc.
#SBATCH -t 02:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
./MatMul 4 1
./MatMul 5 1
./MatMul 6 1
./MatMul 7 1
./MatMul 8 8
./MatMul 9 8
./MatMul 10 8
./MatMul 11 8
./MatMul 12 8
./MatMul 13 8
