#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <time.h>
#include <sys/time.h>

using namespace std;

int GRANULARITY = 0;

vector<vector<int> > generateRandomMatrix(int n) {
	vector<vector<int> > mat(n, vector<int>(n, 0));
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			mat[i][j] = rand() % 1000 + 1;
	return mat;
}


void writeMatrixToFile(vector<vector<int> > mat, string filename) {
	ofstream f(filename.c_str());
	for(vector<vector<int> >::const_iterator it = mat.begin(); it != mat.end(); it++) {
		for(vector<int>::const_iterator it2 = it->begin(); it2 != it->end(); /Users/prasoon/Documents/SpringSemester/ParallelProgramming/parallel_prog_supercomputers/divide_and_conquer_matrix_multiplication/problem_1/divide_and_conquer_varying_cores/jobscript_P_variations.shit2++) {
			f << *it2 << ' ';/Users/prasoon/Documents/SpringSemester/ParallelProgramming/parallel_prog_supercomputers/divide_and_conquer_matrix_multiplication/problem_1/divide_and_conquer_varying_cores/jobscript_P_variations.sh
		}
		f << '\n';
	}
}

void displayMatrix(vector<vector<int> >& mat) {
	for(vector<vector<int> >::const_iterator it = mat.begin(); it != mat.end(); it++) {
		for(vector<int>::const_iterator it2 = it->begin(); it2 != it->end(); it2++) {
			cout<<*it2<<' ';
		}
		cout<<endl;
	}
}

void multiplyMatrixIterative(int n, vector<vector<int> >& matX, vector<vector<int> >& matY, vector<vector<int> >& matZ, pair<int, int> startX, pair<int, int> startY, pair<int, int> startZ) {
	for (int i = 0; i < n; i++)
		for (int k = 0; k < n; k++)
			for (int j = 0; j < n; j++)
				matZ[i + startZ.first][j + startZ.second] = matZ[i + startZ.first][j + startZ.second] + (matX[i + startX.first][startX.second + k] * matY[startY.first + k][startY.second + j]);
}

void multiplyMatrixDivideAndConquer(int n, vector<vector<int> >& matX, vector<vector<int> >& matY, vector<vector<int> >& matZ, pair<int, int> startX, pair<int, int> startY, pair<int, int> startZ) {
	if (n == GRANULARITY) {
		multiplyMatrixIterative(n, matX, matY, matZ, startX, startY, startZ);
		return;
	}
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, startX, startY, startZ);
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, startX, make_pair(startY.first, startY.second + n/2), make_pair(startZ.first, startZ.second + n/2));
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second), startY, make_pair(startZ.first + n/2, startZ.second));
	multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second), make_pair(startY.first, startY.second + n/2), make_pair(startZ.first + n/2, startZ.second + n/2));

    cilk_sync;

    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first, startX.second + n/2), make_pair(startY.first + n/2, startY.second), startZ);
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first, startX.second + n/2), make_pair(startY.first + n/2, startY.second + n/2), make_pair(startZ.first, startZ.second + n/2));
    cilk_spawn multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startY.first + n/2, startY.second), make_pair(startZ.first + n/2, startZ.second));
	multiplyMatrixDivideAndConquer(n/2, matX, matY, matZ, make_pair(startX.first + n/2, startX.second + n/2), make_pair(startY.first + n/2, startY.second + n/2), make_pair(startZ.first + n/2, startZ.second + n/2));

	cilk_sync;
}




vector<vector<int> > inputMatrix(int n) {
	vector<vector<int> > mat(n, vector<int>(n, 0));
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			cin>>mat[i][j];
	return mat;
}


int main(int argc, char* argv[]) {
	string filename;
	double execution_time;
	struct timeval start, end;
	if (argc < 4 || atoi(argv[1]) < 1 || atoi(argv[2]) < 0 || atoi(argv[2]) > atoi(argv[1]) || atoi(argv[3]) < 1) {
		cout<<"[Usage]: matMul <n :: 2^n => matrix edge> <m :: 2^m => base_case_matrix_edge_length> <max_cilk_workers>"<<endl;
		return 1;
	}
	int n = pow(2, atoi(argv[1]));
	GRANULARITY = pow(2, atoi(argv[2]));
	srand(static_cast<int>(time(0)));
	vector<vector<int> > matX = generateRandomMatrix(n);
	vector<vector<int> > matY = generateRandomMatrix(n);
	vector<vector<int> > matZ(n, vector<int>(n, 0));
	writeMatrixToFile(matX, "PARALLEL_matX.txt");
	writeMatrixToFile(matY, "PARALLEL_matY.txt");
	vector<vector<int> > matW(n, vector<int>(n, 0));
	__cilkrts_set_param("nworkers", argv[3]);
	gettimeofday(&start, NULL);
	multiplyMatrixDivideAndConquer(n, matX, matY, matW, make_pair(0, 0), make_pair(0, 0), make_pair(0, 0));
	gettimeofday(&end,NULL);
	execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
	cout<<"n = "<<argv[1]<<", m = "<<argv[2]<<", p = "<<argv[3]<<", Execution Time(Recursive D&C parallel solution): "<<execution_time<<endl;
	filename = "PARALLEL_matZ_recursive.txt";
	writeMatrixToFile(matW, filename);
	return 0;
}

