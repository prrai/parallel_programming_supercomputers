#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 32              # total number of mpi tasks requested
#SBATCH -p development         # queue (partition) -- normal, development, etc.
#SBATCH -t 02:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
./MatMul 13 2
./MatMul 13 3
./MatMul 13 4
./MatMul 13 5
./MatMul 13 7
./MatMul 13 8
./MatMul 13 9
./MatMul 13 10
./MatMul 13 11
./MatMul 13 12
