#!/bin/sh
#BATCH -J first_job           # job name
#SBATCH -o first.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 32              # total number of mpi tasks requested
#SBATCH -p normal         # queue (partition) -- normal, development, etc.
#SBATCH -t 00:30:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-user=prrai@cs.stonybrook.edu
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
./matMulB 4
./matMulB 5
./matMulB 6
./matMulB 7
./matMulB 8
./matMulB 9
./matMulB 10
./matMulB 11
./matMulB 12
./matMulB 13
./matMulE 4
./matMulE 5
./matMulE 6
./matMulE 7
./matMulE 8
./matMulE 9
./matMulE 10
./matMulE 11
./matMulE 12
./matMulE 13
