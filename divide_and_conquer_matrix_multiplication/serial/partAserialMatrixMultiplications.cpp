#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <time.h>
#include <sys/time.h>

using namespace std;

vector<vector<int> > generateRandomMatrix(int n) {
	vector<vector<int> > mat(n, vector<int>(n, 0));
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			mat[i][j] = rand() % 1000 + 1;
	return mat;
}

void writeMatrixToFile(vector<vector<int> > mat, string filename) {
	ofstream f(filename.c_str());
	for(vector<vector<int> >::const_iterator it = mat.begin(); it != mat.end(); it++) {
		for(vector<int>::const_iterator it2 = it->begin(); it2 != it->end(); it2++) {
			f << *it2 << ' ';
		}
		f << '\n';
	}
}

void multiplyMatrix(int n, char variant, vector<vector<int> >& matX, vector<vector<int> >& matY) {
	double execution_time;
	struct timeval start, end;
	string filename;
	vector<vector<int> > matZ(n, vector<int>(n, 0));
	switch(variant) {
		case 'A':
			gettimeofday(&start, NULL);
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
					for (int k = 0; k < n; k++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
		case 'B':
			gettimeofday(&start, NULL);
			for (int i = 0; i < n; i++)
				for (int k = 0; k < n; k++)
					for (int j = 0; j < n; j++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		case 'C':
			gettimeofday(&start, NULL);
			for (int j = 0; j < n; j++)
				for (int i = 0; i < n; i++)
					for (int k = 0; k < n; k++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		case 'D':
			gettimeofday(&start, NULL);
			for (int j = 0; j < n; j++)
				for (int k = 0; k < n; k++)
					for (int i = 0; i < n; i++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		case 'E':
			gettimeofday(&start, NULL);
			for (int k = 0; k < n; k++)
				for (int i = 0; i < n; i++)
					for (int j = 0; j < n; j++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		case 'F':
			gettimeofday(&start, NULL);
			for (int k = 0; k < n; k++)
				for (int j = 0; j < n; j++)
					for (int i = 0; i < n; i++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		default:
			cout<<"Invalid variant, exit!"<<endl;
			exit(EXIT_FAILURE);
	}
	cout<<"Matrix edge: "<<n<<", Variant:"<<variant<<", Execution Time: "<<execution_time<<endl;
	filename = "matZ_variant_";
	filename += variant;
	filename += ".txt";
	writeMatrixToFile(matZ, filename);
}

int main(int argc, char* argv[]) {
	if (argc < 2 || atoi(argv[1]) < 1) {
		cout<<"[Usage]: matMul <m :: 2^m => matrix edge>"<<endl;
		return 1;
	}
	int n = pow(2, atoi(argv[1]));
	srand(static_cast<int>(time(0)));
	vector<vector<int> > matX = generateRandomMatrix(n);
	vector<vector<int> > matY = generateRandomMatrix(n);
	writeMatrixToFile(matX, "matX.txt");
	writeMatrixToFile(matY, "matY.txt");
	multiplyMatrix(n, 'A', matX, matY);
	multiplyMatrix(n, 'B', matX, matY);
	multiplyMatrix(n, 'C', matX, matY);
	multiplyMatrix(n, 'D', matX, matY);
	multiplyMatrix(n, 'E', matX, matY);
	multiplyMatrix(n, 'F', matX, matY);
	return 0;
}

