#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <time.h>
#include <sys/time.h>

using namespace std;

vector<vector<int> > generateRandomMatrix(int n) {
	vector<vector<int> > mat(n, vector<int>(n, 0));
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			mat[i][j] = rand() % 1000 + 1;
	return mat;
}

void writeMatrixToFile(vector<vector<int> > mat, string filename) {
	ofstream f(filename.c_str());
	for(vector<vector<int> >::const_iterator it = mat.begin(); it != mat.end(); it++) {
		for(vector<int>::const_iterator it2 = it->begin(); it2 != it->end(); it2++) {
			f << *it2 << ' ';
		}
		f << '\n';
	}
}

void multiplyMatrix(int n, char variant, vector<vector<int> >& matX, vector<vector<int> >& matY, const char* workers) {
	double execution_time;
	struct timeval start, end;
	string filename;
	vector<vector<int> > matZ(n, vector<int>(n, 0));
	string order = "ikj";
	string parallel_loops;
	switch(variant) {
		case '1':
			gettimeofday(&start, NULL);
			parallel_loops = "i";
			cilk_for (int i = 0; i < n; i++)
				for (int k = 0; k < n; k++)
					for (int j = 0; j < n; j++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		case '2':
			gettimeofday(&start, NULL);
			parallel_loops = "j";
			for (int i = 0; i < n; i++)
				for (int k = 0; k < n; k++)
					cilk_for (int j = 0; j < n; j++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		case '3':
			gettimeofday(&start, NULL);
			parallel_loops = "ij";
			cilk_for (int i = 0; i < n; i++)
				for (int k = 0; k < n; k++)
					cilk_for (int j = 0; j < n; j++)
						matZ[i][j] = matZ[i][j] + (matX[i][k] * matY[k][j]);
			gettimeofday(&end,NULL);
			execution_time = (end.tv_sec+(double)end.tv_usec/1000000) - (start.tv_sec+(double)start.tv_usec/1000000);
			break;
		default:
			cout<<"Invalid variant, exit!"<<endl;
			exit(EXIT_FAILURE);
	}
	cout<<"Order: "<<order<<", Variant: "<<variant<<", Parallel Loops: "<<parallel_loops<<", Workers: "<<workers<<", Execution Time: "<<
	execution_time<<endl;
	filename = "SerialVariantB_matZ_variant_";
	filename += variant;
	filename += "_Workers_";
	filename += workers;
	filename += ".txt";
	writeMatrixToFile(matZ, filename);
}

int main(int argc, char* argv[]) {
	if (argc < 2 || atoi(argv[1]) < 1 || atoi(argv[2]) < 1) {
		cout<<"[Usage]: matMul <m :: 2^m => matrix edge> <max_cilk_workers>"<<endl;
		return 1;
	}
	int n = pow(2, atoi(argv[1]));
	srand(static_cast<int>(time(0)));
	vector<vector<int> > matX = generateRandomMatrix(n);
	vector<vector<int> > matY = generateRandomMatrix(n);
	writeMatrixToFile(matX, "SerialVariantB_matX.txt");
	writeMatrixToFile(matY, "SerialVariantB_matY.txt");
	__cilkrts_set_param("nworkers", argv[2]);
	multiplyMatrix(n, '1', matX, matY, argv[2]);
	multiplyMatrix(n, '2', matX, matY, argv[2]);
	multiplyMatrix(n, '3', matX, matY, argv[2]);
	return 0;
}

