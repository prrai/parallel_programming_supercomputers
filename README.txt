
-------------------------------------------------------------------------------
In this project, we implemented the following algorithms for parallel processing
on Supercomputers Stampede and Comet:
-------------------------------------------------------------------------------

1. Matrix Multiplication (CILK, STAMPEDE):
	A. Serial
	B. Parallel loops for serial.
	C. Divide and Conquer.

2. Parenthesis Problem (CILK, STAMPEDE):
	A. Serial
	B. Parallel Divide and Conquer.

3. Sieve of Eratosthenes (CILK, STAMPEDE):
	A. Parallel Loops

4. Deterministic Parallel Connected Components (CILK, STAMPEDE).

5. Randomized Parallel Connected Components (CILK, STAMPEDE).

6. Canon Matrix Multiplication (MPI, STAMPEDE, COMET)
	A. Single Processor Multiple Cores
	B. Multiple Processors Multiple Cores
	C. Both A and B in slave-master model.

For the 6 problems, analyzed effects of:
	A. Changing number of cores
	B. Recursion base case granularity
	C. Changing problem size